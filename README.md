# Web đặt vé xem phim và quản lý phim, quản lý người dùng

Dự án này là bài tập từ trung tâm tin học cybersoft.
Dự án được xây dựng từ React library.

### Tham khảo

Dự án có tham khảo luồng và giao diện của trang [StickMovie]().

### Mô tả dự án và thông tin nội dung thành viên thực hiện:

Thành viên thực hiện dự án: Quan Vĩ Nguyên + Trần Hoàng Vũ

Khách hàng có thể: 

- Đăng kí , đăng nhập
- Xem những bộ phim có suất chiếu theo ngày hiện tại và những ngày tiếp theo
- Xem trailer phim
- Xem vé xem phim theo ngày hiện tại và những ngày tiếp theo
- Lọc lịch chiếu phim theo cụm rạp, xem chi tiết thông tin phim
- Đặt ghế xem phim, đặt đồ ăn, thanh toán
- Xem thông tin người dùng, đặt vé và thay đổi thông tin người dùng

Người quản trị có thể:

- Quản lý (thêm, xóa, cập nhật) thông tin lịch chiếu phim
- Quản lý (thêm, xóa, cập nhật) thông tin chi tiết của phim
- Quản lý (thêm, xóa, cập nhật) thông tin người dùng trên trang (bao gồm admin lẫn khách hàng)

### Links

- Github: [https://gitlab.com/Nguyen0103/captone_react_movie_bc31_team15]
         (https://gitlab.com/Nguyen0103/captone_react_movie_bc31_team15)
- Demo: []()
- Deloy: []()

### Các công nghệ sử dụng

-ReactJS
-Material UI
-@Redux/Toolkit

## Author

- Github: [Quan Vĩ Nguyên](https://gitlab.com/Nguyen0103/captone_react_movie_bc31_team15) - Email:[slotdi0103@gmail.com]